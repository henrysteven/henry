/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LecturaEscritura;

import Personajes.Usuario;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static javafx.scene.input.KeyCode.T;
import pokemons.*;

/**
 *
 * @author Henry
 */
public class MetodosGuardado {

    public static void Leer() throws IOException {
        List<String> lista = Files.readAllLines(Paths.get("Fuente/pokemon.csv"));
        String[] nombresl = lista.get(0).split(",");
        List<String> nombre2 = Arrays.asList(nombresl).subList(1, 18);
        for (int i = 1; i < lista.size(); i++) {
            String l = lista.get(i).replace("[", "").replace("'", "").replace("\"", "");
            String[] lista1 = l.split("]");
            String[] habilidades = lista1[0].replace(" ", "").split(",");
            String[] valores = lista1[1].split(",");
            if (Integer.parseInt(valores[27]) == 1) {

                ArrayList<Fortaleza> fortalezas = new ArrayList<>();
                ArrayList<Habilidad> habilidades1 = new ArrayList<>();
                for (String s : habilidades) {
                    Habilidad habilidad = new Habilidad(s,10,10);
                    habilidades1.add(habilidad);
                }
                for (int j = 0; j < nombre2.size(); j++) {
                    try {
                        Fortaleza f = new Fortaleza(nombre2.get(j).replace("against_", ""), Double.parseDouble(valores[j]));
                        fortalezas.add(f);
                        
                    } catch (Exception e) {
                    }
                }

                Pokemon pokemon = new Pokemon(Integer.parseInt(valores[19]), Integer.parseInt(valores[20]), Integer.parseInt(valores[21]), Integer.parseInt(valores[22]),Integer.parseInt(valores[22]), valores[23], Integer.parseInt(valores[24]), valores[25], valores[26], Integer.parseInt(valores[27]), Boolean.parseBoolean(valores[28]), fortalezas, habilidades1, 1,0);
                Pokemon.pokemons.add(pokemon);

            }
        }
    }

    public static void guardarLista(String nombreArchivo, ArrayList<Usuario> d) {
        File fichero = new File(nombreArchivo);
        FileOutputStream fos;
        ObjectOutputStream oos;
        try {
            fos = new FileOutputStream(fichero);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(d);
            oos.close();
            System.out.println("Se guardo");
        } catch (FileNotFoundException e) {
            System.out.println("Error es la localizacion del archivo");
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public static void leerfichero(String nombreArchivo, ArrayList<Usuario> d) throws ClassNotFoundException {
        FileInputStream fis;
        ObjectInputStream ois;
        try {
            fis = new FileInputStream(nombreArchivo);
            ois = new ObjectInputStream(fis);
            d = (ArrayList<Usuario>) ois.readObject();
            Usuario.usuarios = d;
            System.out.println("bueno");
        } catch (FileNotFoundException e) {
            System.out.println("No existe");
        } catch (IOException e) {
            System.out.println("Error");
        }
    }
}
