/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Personajes;


import LecturaEscritura.MetodosGuardado;
import java.util.ArrayList;
import java.util.Random;
import pokemons.Pokemon;

/**
 *
 * @author kexbl
 */
public class Usuario {
    
    public static Usuario u = new Usuario("Henry", llenarPo());
    public static ArrayList<Usuario> usuarios = new ArrayList<>();
    
    public Usuario() { 
    }

    private String Nombre;
    private int pokeballs=0;
    private int monedas=0;
    private ArrayList<Pokemon> pokemonsJugador = new ArrayList<>();

    
    public Usuario(String Nombre, ArrayList<Pokemon> pokemonsJugador) {
        this.Nombre = Nombre;
        this.pokemonsJugador = pokemonsJugador;
    }

    public Usuario(String henry) {
        this.Nombre = Nombre;
    }

    public static ArrayList<Pokemon> llenarPo() {
        ArrayList<Pokemon> pokemonsJugador = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Pokemon p = ramdonPokemon();
            pokemonsJugador.add(p);
        }

        return pokemonsJugador;
    }

    public static Pokemon ramdonPokemon() {
        Random r = new Random();
        int x = r.nextInt(Pokemon.pokemons.size());
        Pokemon p = Pokemon.pokemons.get(x);
        return p;
    }

    /*public Usuario getU() {
        return u;
    }*/
    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public int getMonedas() {
        return monedas;
    }

    public void setMonedas(int monedas) {
        this.monedas = monedas;
    }

    public int getPokeballs() {
        return pokeballs;
    }

    public void setPokeballs(int pokeballs) {
        this.pokeballs = pokeballs;
    }

    

    public ArrayList<Pokemon> getPokemonsJugador() {
        return pokemonsJugador;
    }

    public void setPokemonsJugador(ArrayList<Pokemon> pokemonsJugador) {
        this.pokemonsJugador = pokemonsJugador;
    }

    @Override
    public String toString() {
        return "Usuario{" + "Nombre=" + Nombre + ", pokemonsJugador=" + pokemonsJugador + '}';
    }
}
