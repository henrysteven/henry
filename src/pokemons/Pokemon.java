/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemons;

import java.util.ArrayList;
import pokemons.*;

/**
 *
 * @author Henry
 */
public class Pokemon {
    public static ArrayList<Pokemon> pokemons = new ArrayList<>();
    private int atk;
    private int captureRate;
    private int defensa;
    private int hp;
    private int hp2;
    private String nombre;
    private int numeroPokedex;
    private String tipo1;
    private String tipo2;
    private int generacion;
    private boolean legendario;
    private ArrayList<Fortaleza> fortalezas = new ArrayList<>();
    private ArrayList<Habilidad> hablilidades= new ArrayList<>();
    private int nivel =1;
    private int exp = 0;

    public Pokemon(int atk, int captureRate, int defensa, int hp,int hp2, String nombre, int numeroPokedex, String tipo1, String tipo2, int generacion, boolean legendario,ArrayList<Fortaleza> fortalezas,ArrayList<Habilidad> hablilidades,int nivel,int exp) {
        this.hp2=atk;
        this.atk = atk;
        this.captureRate = captureRate;
        this.defensa = defensa;
        this.hp = hp;
        this.nombre = nombre;
        this.numeroPokedex = numeroPokedex;
        this.tipo1 = tipo1;
        this.tipo2 = tipo2;
        this.generacion = generacion;
        this.legendario = legendario;
        this.fortalezas=fortalezas;
        this.hablilidades=hablilidades;
        this.nivel= nivel;
        this.exp = exp;
    }

    public int getAtk() {
        return atk;
    }

    public void setAtk(int atk) {
        this.atk = atk;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getCaptureRate() {
        return captureRate;
    }

    public void setCaptureRate(int captureRate) {
        this.captureRate = captureRate;
    }

    public int getDefensa() {
        return defensa;
    }

    public void setDefensa(int defensa) {
        this.defensa = defensa;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNumeroPokedex() {
        return numeroPokedex;
    }

    public void setNumeroPokedex(int numeroPokedex) {
        this.numeroPokedex = numeroPokedex;
    }

    public String getTipo1() {
        return tipo1;
    }

    public void setTipo1(String tipo1) {
        this.tipo1 = tipo1;
    }

    public String getTipo2() {
        return tipo2;
    }

    public void setTipo2(String tipo2) {
        this.tipo2 = tipo2;
    }

    public int getGeneracion() {
        return generacion;
    }

    public void setGeneracion(int generacion) {
        this.generacion = generacion;
    }

    public boolean isLegendario() {
        return legendario;
    }

    public void setLegendario(boolean legendario) {
        this.legendario = legendario;
    }

    public static void setPokemons(ArrayList<Pokemon> pokemons) {
        Pokemon.pokemons = pokemons;
    }

    public static ArrayList<Pokemon> getPokemons() {
        return pokemons;
    }

    public ArrayList<Fortaleza> getFortalezas() {
        return fortalezas;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public void setFortalezas(ArrayList<Fortaleza> fortalezas) {
        this.fortalezas = fortalezas;
    }

    public ArrayList<Habilidad> getHablilidades() {
        return hablilidades;
    }

    public void setHablilidades(ArrayList<Habilidad> hablilidades) {
        this.hablilidades = hablilidades;
    }

    @Override
    public String toString() {
        return "Pokemon{" + "atk=" + atk + ", captureRate=" + captureRate + ", defensa=" + defensa + ", hp=" + hp + ", nombre=" + nombre + ", numeroPokedex=" + numeroPokedex + ", tipo1=" + tipo1 + ", tipo2=" + tipo2 + ", generacion=" + generacion + ", legendario=" + legendario + ", fortalezas=" + fortalezas + ", hablilidades=" + hablilidades + ", nivel=" + nivel + '}';
    }

    
    
    
}
