/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemons;

/**
 *
 * @author Henry
 */
public class Fortaleza {
    private String fortalezaTipo;
    private Double valor;

    public Fortaleza(String fortalezaTipo, Double valor) {
        this.fortalezaTipo = fortalezaTipo;
        this.valor = valor;
    }

    public String getFortalezaTipo() {
        return fortalezaTipo;
    }

    public void setFortalezaTipo(String fortalezaTipo) {
        this.fortalezaTipo = fortalezaTipo;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "Fortaleza{" + "fortalezaTipo=" + fortalezaTipo + ", valor=" + valor + '}';
    }
    
}
