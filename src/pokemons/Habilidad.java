/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemons;

/**
 *
 * @author Henry
 */
public class Habilidad {
    private String nombreAtaque;
    private int valorataque;
    private int valorATK2;

    public Habilidad(String nombreAtaque) {
        this.nombreAtaque = nombreAtaque;
    }

    public Habilidad(String nombreAtaque, int valorataque, int valorATK2) {
        this.nombreAtaque = nombreAtaque;
        this.valorataque = valorataque;
        this.valorATK2 =valorATK2;
    }

    public int getValorATK2() {
        return valorATK2;
    }

    public void setValorATK2(int valorATK2) {
        this.valorATK2 = valorATK2;
    }

    public int getValorataque() {
        return valorataque;
    }

    public void setValorataque(int valorataque) {
        this.valorataque = valorataque;
    }

    @Override
    public String toString() {
        return "Habilidad{" + "nombreAtaque=" + nombreAtaque + '}';
    }

    public String getNombreAtaque() {
        return nombreAtaque;
    }

    public void setNombreAtaque(String nombreAtaque) {
        this.nombreAtaque = nombreAtaque;
    }
    
}
