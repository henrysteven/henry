/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batalla;

import LecturaEscritura.MetodosGuardado;
import Personajes.Usuario;
import static Personajes.Usuario.u;
import static Personajes.Usuario.usuarios;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pokemons.Pokemon;

/**
 *
 * @author Henry
 */
public class Batalla1 extends Application {

    public static Stage principal = new Stage();

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument1.fxml"));

        Scene scene = new Scene(root);

        principal.setScene(scene);

        principal.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException {
        String rutaUsuario = "Usuarios3.bin";

        ArrayList<Pokemon> pokemones = Pokemon.pokemons;
        try {
            MetodosGuardado.Leer();

            if (usuarios.size() != 1) {
                usuarios.add(u);
                MetodosGuardado.guardarLista(rutaUsuario, Usuario.usuarios);
            } else {
                MetodosGuardado.leerfichero(rutaUsuario, Usuario.usuarios);
            }
        } catch (IOException ex) {
        }
        launch();
    }

}
