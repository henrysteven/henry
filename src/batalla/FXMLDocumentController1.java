/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batalla;

import Personajes.Usuario;
import static Personajes.Usuario.llenarPo;
import com.sun.xml.internal.ws.api.message.saaj.SAAJFactory;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import pokemons.Fortaleza;
import pokemons.Habilidad;
import pokemons.Pokemon;

/**
 *
 * @author Henry
 */
public class FXMLDocumentController1 implements Initializable {

    @FXML
    private HBox cajaAtkArriba;

    @FXML
    private Button ataque1;

    @FXML
    private Button ataque2;

    @FXML
    private VBox CajaAtk;

    @FXML
    private Button ataque3;

    @FXML
    private Button ataque4;

    @FXML
    private Pane panel;

    @FXML
    private FlowPane panelPokemonsJugador;

    @FXML
    private HBox cajaAtaqueAbajo;

    @FXML
    ImageView ima;

    @FXML
    Label l;

    @FXML
    private Pane paneIzq;

    @FXML
    private ImageView pokemonUsuario = new ImageView();

    @FXML
    private Label nombreP;

    @FXML
    private Pane paneDer;

    @FXML
    private ImageView adversarioP;
    @FXML
    private Label cambio;

    @FXML
    private Button pokeballs;
    @FXML
    private Label monedas;

    public Pokemon pB = Pokemon.pokemons.get(5);

    void nombrarAtaque(Pokemon pokemon) {

        for (int i = 0; i < pokemon.getHablilidades().size(); i++) {
            String nombreAtaque = pokemon.getHablilidades().get(i).getNombreAtaque();
            int v = pokemon.getHablilidades().get(i).getValorataque();

            if (i == 1) {
                ataque1.setText(nombreAtaque + "-" + v);

                if (v == 0) {
                    ataque1.setDisable(true);
                } else if (pokemon.getHp() == 0) {
                    ataque1.setDisable(true);
                }
            }
            if (i == 2) {
                ataque2.setText(nombreAtaque + "-" + v);
                if (v == 0) {
                    ataque2.setDisable(true);
                } else if (pokemon.getHp() == 0) {
                    ataque2.setDisable(true);
                }
            }
            if (i == 3) {
                ataque3.setText(nombreAtaque + "-" + v);
                if (v == 0) {
                    ataque3.setDisable(true);
                } else if (pokemon.getHp() == 0) {
                    ataque3.setDisable(true);
                }
            }
            if (i == 4) {
                ataque4.setText(nombreAtaque + "-" + v);
                if (v == 0) {
                    ataque4.setDisable(true);
                } else if (pokemon.getHp() == 0) {
                    ataque4.setDisable(true);
                }
            }
        }
        if (ataque1.getText().equals("Ataque 1")) {
            ataque1.setVisible(false);
        }
        if (ataque2.getText().equals("Ataque 2")) {
            ataque2.setVisible(false);
        }
        if (ataque3.getText().equals("Ataque 3")) {
            ataque3.setVisible(false);
        }
        if (ataque4.getText().equals("Ataque 4")) {
            ataque4.setVisible(false);
        }

    }

    void llenarVbox(Pokemon p, Pane pane) {

        VBox vBoxpane = new VBox();

        Label nivel = new Label("NIVEL: " + p.getNivel());
        nivel.setFont(Font.font("Cambria", 25));
        nivel.setTextFill(Color.BLACK);
        Label exp = new Label("EXPERIENCIA : " + p.getExp());
        exp.setFont(Font.font("Cambria", 20));
        exp.setTextFill(Color.BLUE);
        Label atk = new Label("ATAQUE: " + p.getAtk());
        atk.setFont(Font.font("Cambria", 20));
        atk.setTextFill(Color.GREEN);
        Label defensa = new Label("DEFENSA: " + p.getDefensa());
        defensa.setFont(Font.font("Cambria", 20));
        defensa.setTextFill(Color.BROWN);
        Label hp = new Label("VIDA: " + p.getHp());
        hp.setFont(Font.font("Cambria", 20));
        hp.setTextFill(Color.RED);
        Label tipos = new Label("-Tipos 1: " + p.getTipo1() + " -Tipo 2: " + p.getTipo2());
        tipos.setFont(Font.font("Cambria", 15));
        tipos.setTextFill(Color.BLACK);
        Button boton_fortaleza = new Button("Fortalezas");
        boton_fortaleza.setOnMouseClicked(e -> fortalezas(p));
        Label nombre = new Label("NOMBRE: " + p.getNombre());
        nombre.setFont(Font.font("Times New Roman", 20));
        nombre.setTextFill(Color.BLACK);
        Button confirmarPokemon = new Button("Confirmar");
        confirmarPokemon.setOnMouseClicked(e -> cambiarPokemon(p, Integer.valueOf(cambio.getText()), confirmarPokemon));
        vBoxpane.getChildren().addAll(nombre, nivel, atk, defensa, hp, tipos, boton_fortaleza, confirmarPokemon);
        pane.getChildren().clear();

        pane.getChildren().add(vBoxpane);

    }

    void cambiarPokemon(Pokemon p, int v, Button b) {
        v++;

        if (v != 3) {
            cambio.setText(String.valueOf(v));
            if (p.getHp() == 0) {
                b.setDisable(true);
            }
        } else {
            cambio.setText(String.valueOf(v));
            b.setDisable(true);
        }
    }

    void llenarMonedasPokeballs() {
        pokeballs.setText(String.valueOf(Usuario.u.getPokeballs()));
        monedas.setText(String.valueOf(Usuario.u.getMonedas()));
        if (Usuario.u.getPokeballs() == 0) {
            pokeballs.setDisable(true);
        }

    }

    void fortalezas(Pokemon p) {
        Stage pantallaFortaleza = new Stage();
        VBox fortalezas = new VBox(2.5);
        for (Fortaleza f : p.getFortalezas()) {
            Label fortaleza = new Label("->Fuerte con:" + f.getFortalezaTipo() + "------------Valor: " + f.getValor() + "\n");
            fortaleza.setFont(Font.font("Cambria", 14));
            fortaleza.setTextFill(Color.BLACK);
            fortalezas.getChildren().add(fortaleza);
        }
        Scene scFortaleza = new Scene(fortalezas, 500, 500);
        pantallaFortaleza.setScene(scFortaleza);
        pantallaFortaleza.show();
    }

    void llenarpanelPokemonsJugador() {
        llenarMonedasPokeballs();
        try {
            Button b;
            for (Pokemon p : Usuario.u.getPokemonsJugador()) {
                String s = p.getNombre();
                String ruta = "batalla\\red-blue\\" + Integer.toString(p.getNumeroPokedex()) + ".png";
                ima = new ImageView(new Image(ruta));
                ima.setFitHeight(50);
                ima.setFitWidth(80);
                b = new Button(s, ima);

                b.setOnMouseClicked(e -> {
                    try {
                        programarPokemon(p, pokemonUsuario, this.pB, adversarioP);

                    } catch (MalformedURLException ex) {
                    }
                });

                panelPokemonsJugador.getChildren().add(b);
            }
        } catch (Exception ex) {
        }

    }

    void programarPokemon(Pokemon pA, ImageView pokemonUsuario, Pokemon pB, ImageView adversarioP) throws MalformedURLException {
        try {

            String rutaA = "batalla\\red-blue\\back\\" + Integer.toString(pA.getNumeroPokedex()) + ".png";
            Image imageA = new Image(rutaA, 100, 100, true, true, true);
            pokemonUsuario.setImage(imageA);
            nombrarAtaque(pA);
            String rutaB = "batalla\\red-blue\\" + Integer.toString(pB.getNumeroPokedex()) + ".png";
            Image imageB = new Image(rutaB, 100, 100, true, true, true);
            adversarioP.setImage(imageB);
            nombrarAtaque(pB);
            llenarVbox(pA, this.paneIzq);
            llenarVbox(pB, this.paneDer);
            batallaPokemon(pA, pB);
        } catch (Exception e) {
        }
    }

    void daño(Pokemon pA, Pokemon pB) {
        int z = 0;
        for (Fortaleza f : pA.getFortalezas()) {
            try {
                if (f.equals(pB.getTipo1()) || f.equals(pB.getTipo2())) {
                    int a = (int) (pA.getAtk() / pB.getDefensa());
                    double d = f.getValor() * (a + 10);
                    z = (int) d;
                } else {
                    z = (pA.getAtk() / pB.getDefensa()) + (10);
                }
            } catch (Exception e) {
            } finally {

            }

        }
        pB.setHp(pB.getHp() - 10);

    }

    void metodoTiempo(Pokemon pA, Pokemon pB, Button b) {

        new Thread(new Runnable() {
            public void run() {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        try {

                            b.setDisable(true);
                            for (int i = 0; i < 2; i++) {
                                Thread.sleep(1000);
                                if (i == 1) {
                                    b.setDisable(false);

                                }
                            }

                        } catch (InterruptedException e) {
                        }
                    }
                });

            }
        }).start();

    }
    
    void comprobarVida(){}

    void metodos(Pokemon pA, Pokemon pB) {
        daño(pB, pA);
        llenarVbox(pA, this.paneIzq);
        llenarVbox(pB, this.paneDer);
    }

    void batallaPokemon(Pokemon pA, Pokemon pB) {
        ataque1.setOnMouseClicked(e -> {
            daño(pA, pB);
            metodoTiempo(pA, pB, ataque1);
            metodos(pA, pB);

        });
        ataque2.setOnMouseClicked(e -> {
            daño(pA, pB);
            metodoTiempo(pA, pB, ataque2);
            metodos(pA, pB);

        });
        ataque3.setOnMouseClicked(e -> {
            daño(pA, pB);
            metodoTiempo(pA, pB, ataque3);
            metodos(pA, pB);
        });
        ataque4.setOnMouseClicked(e -> {
            daño(pA, pB);
            metodoTiempo(pA, pB, ataque4);
            metodos(pA, pB);
        });

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        llenarpanelPokemonsJugador();

    }

}
